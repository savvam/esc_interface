/**
 * @file set_pwm.cpp
 * @brief CLI tool to send pwm commands
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 Oct 2019
 */

#include <cmath>
#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include "esc_interface/esc_interface.h"

// to be set by sighandler
volatile sig_atomic_t stop = 0;
void handle_sigint(int s) { stop = true; }

int main(int argc, char const *argv[])
{
  // install sig handler
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = handle_sigint;
  sa.sa_flags = 0; // not SA_RESTART
  sigaction(SIGINT, &sa, NULL);

  // the parameters of the ramp (default is a constant-amplitude ramp)
  uint16_t usecs_low = acl::ESCInterface::PWM_MIN_PULSE_WIDTH;
  uint16_t usecs_high = usecs_low;
  float ramp_secs = 0.0f;

  // the user does not want a ramp
  if (argc >= 2) {
    uint16_t tmp;
    std::istringstream ss(argv[1]);

    if (!(ss >> tmp)) {
      std::cerr << "Invalid number: " << argv[1] << std::endl;
    } else {
      usecs_low = tmp;
      usecs_high = tmp;
    }
  }

  // the user wants a ramp
  if (argc == 4) {

    {
      uint16_t tmp;
      std::istringstream ss(argv[2]);
      if (!(ss >> tmp)) {
        std::cerr << "Invalid number: " << argv[2] << std::endl;
      } else {
        usecs_high = tmp;
      }
    }

    {
      float tmp;
      std::istringstream ss(argv[3]);
      if (!(ss >> tmp)) {
        std::cerr << "Invalid number: " << argv[3] << std::endl;
      } else {
        ramp_secs = std::fabs(tmp);
      }
    }

  }

  constexpr int NUM_PWMS = 8;
  acl::ESCInterface escs(NUM_PWMS);

  bool success = escs.init();
  if (!success) {
    std::cout << "Could not initialize ESC Interface!!" << std::endl;
    // return -1;
  }

  escs.arm();

  if (ramp_secs == 0.0f) {
    // constant pwm value
    std::cout << "Setting PWMs to " << usecs_low << std::endl;

    // command min pwm pulse width
    uint16_t pwm[NUM_PWMS];
    for (int i=0; i<NUM_PWMS; ++i) pwm[i] = usecs_low;

    escs.update(pwm, NUM_PWMS);

    while (!stop);
  } else {
    // pwm ramp
    std::cout << "Ramping PWMs from " << usecs_low  << " to ";
    std::cout << usecs_high << " in " << ramp_secs << " seconds";
    std::cout << std::endl;

    // sampling period (in milliseconds)
    constexpr int Tms = 50;

    // discretization based on user input
    const int N = std::round(ramp_secs / (Tms*1e-3));
    std::cout << N << std::endl;
    const uint16_t step = std::ceil((usecs_high - usecs_low)*1.0f / N);

    std::cout << "Discretization: " << N << " steps of " << step << std::endl;

    // powm pulse width command
    uint16_t pwm[NUM_PWMS];
    uint16_t current_pwm = usecs_low;

    int n = 0;
    while (!stop) {
      // ramp until high
      if (n++ <= N && current_pwm < (usecs_high + step)) {
        for (int i=0; i<NUM_PWMS; ++i) pwm[i] = current_pwm;
        escs.update(pwm, NUM_PWMS);
        std::cout << "pwm: " << current_pwm << std::endl;

        std::this_thread::sleep_for(std::chrono::milliseconds(Tms));
        current_pwm += step;
      }
    }

  }

  return 0;
}