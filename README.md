ESC Interface
=============

Custom ESC interface for Snapdragon Flight 8074 and 8096 boards. Provides a CPU-side shared library for linking to the [ACL Snap autopilot](https://github.com/mit-acl/snap) (ROS).

## Building

1. Clone into the [`sfpro-dev`](https://github.com/mit-acl/sfpro-dev) or [`sf-dev`](https://github.com/mit-acl/sf-dev) `workspace` directory.
2. Update submodules: `git submodule update --init --recursive`.
3. Follow appropriate instructions for building and pushing via `adb`: `./build.sh workspace/esc_interface --load`.

## Implementation Details

While the Krait/Kyro side API is the same for either board, the low-level implementation on the DSP/SLPI is different. These implementations are selected by passing in `QC_SOC_TARGET=APQ8074` or `QC_SOC_TARGET=APQ8096` (see the [Makefile](Makefile)).

### Snapdragon Flight 801 - Eagle 8074

Using the [DSPAL](https://github.com/ATLFlight/dspal), PWM pins are driven with the `/dev/pwm-1` sysfs device on the application DSP (aDSP).

### Snapdragon Flight 820 - Excelsior 8096

On the 8096 board, there is no support for the aDSP. Instead the DSPAL has been extended to support the Sensor Low-Power Island (SLPI). However, [PWM does not seem to be supported](https://github.com/ATLFlight/dspal/blob/master/test/dspal_tester/apps_proc/io_test_suite.c#L51) via the SLPI.

To communicate with the PWM ESCs, the SLPI implementation communicates via I2C to a [PCA9685 board](https://www.adafruit.com/product/815), which can drive up to 16 PWM pins.

**Note:** There is currently a hard-coded offset to use the last 8 pins of the I2C to PWM board. This offset is found in [`sfpro_pwm_imp.c`](adsp_proc/sfpro_pwm_imp.c#L49)

