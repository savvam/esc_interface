/**
 * @file sfpro_pwm_imp.c
 * @brief DSP implementation of PWM/ESC interface for sfpro 8096 using pca9685
 * @author Parker Lusk <plusk@mit.edu>
 * @date 27 June 2019
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"
#include "pca9685.h"

// period (usec) = (1/490)*1e6
// tick resolution = 4096 (12-bit; from PCA9685 datasheet)
// ticks / usec = 4096 / period == 2
#define TICK_PER_USEC 2

int pwm_init(void)
{
  // Open /dev/iic-# (note: using BLSP, not SSC). This is J10.
  const uint8_t iic_devnum = 7;
  bool success = pca9685_init(iic_devnum, PCA9685_DEFAULT_ADDR);
  if (!success) return PWM_ERROR;

  // Give it a full reset for good measure (all outputs LOW)
  pca9685_reset();

  // Most PWM ESCs expect 490 Hz
  pca9685_setPWMFrequency(490);

  LOG_INFO("[pwm] Successfully initialized.");

  return PWM_SUCCESS;
}

// ----------------------------------------------------------------------------

int pwm_update(const uint16_t * pwm, uint16_t len)
{
  uint8_t success = 1;

  // The motor ESCs should be plugged into the PCA9685 board in sequential and
  // ascending order (i.e., motor 1 --> PWM0, etc.). If they are shifted, an
  // offset needs to be applied (i.e,. motor 1 --> PWM8, etc. ; offset = 8).
  const uint8_t OFFSET = 8;

  for (uint8_t i=0; i<len; ++i) {

    // convert usec to PCA9685 ticks
    uint16_t off_ticks = pwm[i] * TICK_PER_USEC;

    success &= pca9685_setPWM(i+OFFSET, 0, off_ticks);
  }

  return (success == 1) ? PWM_SUCCESS : PWM_ERROR;
}

// ----------------------------------------------------------------------------

void pwm_close(void)
{
  // reset device on the way out (all outputs LOW)
  pca9685_reset();

  pca9685_deinit();

  LOG_INFO("[pwm] Closed.");
}
