/**
 * @file pca9685.h
 * @brief C API for the PCA9685 I2C to PWM board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 26 June 2019
 */

#pragma once

#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"

#define PCA9685_DEFAULT_ADDR 0x40

bool pca9685_init(uint8_t i2cdevnum, uint8_t addr);
void pca9685_deinit();

void pca9685_reset();

void pca9685_setPWMFrequency(float freq);
bool pca9685_setPWM(uint8_t num, uint16_t on, uint16_t off);
